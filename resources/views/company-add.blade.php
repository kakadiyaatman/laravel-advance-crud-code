@include('header')
.    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">

                <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                @if(isset($editdata))
                                <div class="card-header">
                                    <h4 class="card-title"><i class="feather icon-edit"></i> Edit Company</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal"  novalidate method="post" action="{{url('company-edit')}}" enctype="multipart/form-data">
                                            @csrf
                                            
                                             <div class="row">
                                                <div class="col-md-6">

                                            

                                                    <div class="form-group">
                                                        <label>Company Name</label>
                                                        <div class="controls">
                                                            <input type="text" name="name"  value="{{$editdata->name}}" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Company Name">
                                                            <input type="hidden" name="id" value="{{$editdata->company_id}}">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Directorate</label>
                                                        <div class="controls">
                                                            <input type="text" name="directorate"  value="{{$editdata->directorate}}" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Directorate">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Group Name</label>
                                                        <div class="controls">
                                                            <input type="text" name="group_name"  value="{{$editdata->group_name}}" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Group Name">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Team</label>
                                                        <div class="controls">
                                                            <input type="text" name="team"  value="{{$editdata->team}}" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Team">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-6">
                                                  
                                                  <div class="form-group">
                                                        <label>Team Leader</label>
                                                        <div class="controls">
                                                            <input type="text" name="team_leader"  value="{{$editdata->team_leader}}" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Team Leader">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Focal Point</label>
                                                        <div class="controls">
                                                            <input type="text" name="focal_point"  value="{{$editdata->focal_point}}" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Focal Point">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Company Notes  @if(!empty($editdata->file_notes))<a href='{{url("public/images/company/$editdata->file_notes")}}' target="_blank"><span class="badge badge-primary"><i class="fa fa-external-link"></i> Open</span></a>@endif</label>
                                                        <div class="controls">
                                                            <input type="file" name="file_notes" class="form-control">
                                                        </div>
                                                    </div>      
                                                
                                                    
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </form>
                                    </div>
                                </div>
                                @else
                                <div class="card-header">
                                    <h4 class="card-title"><i class="feather icon-plus"></i> Add Company</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal"  novalidate method="post" action="{{url('company-add')}}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">

                                            

                                                    <div class="form-group">
                                                        <label>Company Name</label>
                                                        <div class="controls">
                                                            <input type="text" name="name" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Company Name">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Directorate</label>
                                                        <div class="controls">
                                                            <input type="text" name="directorate" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Directorate">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Group Name</label>
                                                        <div class="controls">
                                                            <input type="text" name="group_name" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Group Name">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Team</label>
                                                        <div class="controls">
                                                            <input type="text" name="team" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Team">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-6">
                                                  
                                                  <div class="form-group">
                                                        <label>Team Leader</label>
                                                        <div class="controls">
                                                            <input type="text" name="team_leader" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Team Leader">
                                                        </div>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>Focal Point</label>
                                                        <div class="controls">
                                                            <input type="text" name="focal_point" class="form-control" data-validation-required-message="This field is required" placeholder="Enter Focal Point">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Company Notes </label>
                                                        <div class="controls">
                                                            <input type="file" name="file_notes" class="form-control" data-validation-required-message="This field is required" required="">
                                                        </div>
                                                    </div>       
                                                
                                                    
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                    </div>
                                </div>
                                @endif   
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@include('footer')