@include('header')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">

                <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"><i class="feather icon-upload"></i> Import Info From Excel</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        @if(Session::has('message'))
                                            @if(Session('status')==1)
                                            <p class="alert alert-success">{{ Session('message')}}</p>
                                            @else
                                            <p class="alert alert-danger">{{ Session('message')}}</p>
                                            @endif
                                        @endisset
                                        <form class="form-horizontal"  novalidate method="post" action="{{url('import-export')}}" enctype="multipart/form-data" name="importform">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Choose Excel Sheet</label>
                                                        <div class="controls">
                                                            <input type="file" name="import_file" class="form-control" data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

 <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"><i class="feather icon-download"></i> Export Info Into Excel</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{url('export-data')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="type" value="{{session('type')}}">
                                            <button type="submit" class="btn btn-primary">Export Excel</button>
                                        </form>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->
@include('footer')