   <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="#" target="_blank">HRMS,</a>All rights Reserved</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{url('app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{url('app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/extensions/tether.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{url('app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{url('app-assets/js/core/app.js')}}"></script>
    <script src="{{url('app-assets/js/scripts/components.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{url('app-assets/js/scripts/pages/dashboard-analytics.js')}}"></script>
    <!-- <script src="{{url('app-assets/js/scripts/datatables/datatable.js')}}"></script> -->
    <script src="{{url('app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="{{url('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{url('app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
    <!-- <script src="{{url('app-assets/js/scripts/extensions/sweet-alerts.min.js')}}"></script> -->
    <!-- END: Page JS-->
    <script type="text/javascript">
         $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         function deleteData(table,id){
          Swal.fire({
            title:"Are you sure?",
            text:"You won't be able to revert this!",
            type:"warning",
            showCancelButton:!0,
            confirmButtonColor:"#3085d6",
            cancelButtonColor:"#d33",
            confirmButtonText:"Yes, delete it!",
            confirmButtonClass:"btn btn-primary",
            cancelButtonClass:"btn btn-danger ml-1",
            buttonsStyling:!1})
          .then(
            function(t){
                if(t.value){
                    $.ajax({
                        method:'POST', 
                        url:"{{url('deletedata')}}",
                        data:"_token={{csrf_token()}}&id="+id+"&table="+table,
                        success:function(data){
                          if(data){
                            Swal.fire({ 
                                type:"success",
                                title:"Deleted!",
                                text:"Your record has been deleted.",
                                timer: 1000,
                                confirmButtonClass:"btn btn-success"});
                            $("#rowtr"+id).slideUp();
                        }else{
                           Swal.fire({type:"danger",title:"Not Deleted!",text:"Your record has been not deleted.",timer: 1000,confirmButtonClass:"btn btn-danger"});
                       }
                   }
               });

                }else{
                    Swal.fire({type:"danger",title:"Not Deleted!",text:"Your record has been not deleted.",timer: 1000,confirmButtonClass:"btn btn-danger"});
                }
            });

      }
    $(document).ready(function() {
        $('.table').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    } );



    //active-block
    function activeblock(table,id,status)
    {
        Swal.fire({
            
            title:"Are you sure?",
            text:"You won't be able to revert this!",
            type:"warning",
            showCancelButton:!0,
            confirmButtonColor:"#3085d6",
            cancelButtonColor:"#d33",
            confirmButtonText:"Yes, Change Status!",
            confirmButtonClass:"btn btn-primary",
            cancelButtonClass:"btn btn-danger ml-1",
            buttonsStyling:!1})
        .then(
            function(t){
                if(t.value){
                    $.ajax({
                        method:'POST',
                        url:"{{url('activeblock')}}",
                        data:"_token={{csrf_token()}}&id="+id+"&status="+status+"&table="+table,
                        success:function(data){
                            if(data){
                                if(status==0)       
                                {
                                    if(table=="contractor"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('contractor',"+id+",1)");
                                    }else if(table=="employee"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('employee',"+id+",1)");
                                    }else if(table=="company"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('company',"+id+",1)");
                                    }else if(table=="contract"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('contract',"+id+",1)");
                                    }else if(table=="job"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('job',"+id+",1)");
                                    }else if(table=="staff"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('staff',"+id+",1)");
                                    }else if(table=="standard-job"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('standard-job',"+id+",1)");
                                    }else if(table=="exam-question"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('exam-question',"+id+",1)");
                                    }else if(table=="candidate"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('candidate',"+id+",1)");
                                    }
                                }
                                else
                                {
                                    if(table=="contractor"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('contractor',"+id+",0)");
                                    }else if(table=="employee"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('employee',"+id+",0)");
                                    }else if(table=="company"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('company',"+id+",0)");
                                    }else if(table=="contract"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('contract',"+id+",0)");
                                    }else if(table=="job"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('job',"+id+",0)");
                                    }else if(table=="staff"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('staff',"+id+",0)");
                                    }else if(table=="standard-job"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('standard-job',"+id+",0)");
                                    }else if(table=="exam-question"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('exam-question',"+id+",0)");
                                    }else if(table=="candidate"){
                                        $('#switch_status'+id).attr('onchange',"activeblock('candidate',"+id+",0)");
                                    }
                                }
                                Swal.fire({
                                    type:"success",
                                    title:"Changed!",
                                    text:"Status has been changed.",
                                    timer: 1000,
                                    confirmButtonClass:"btn btn-success"});
                            }else{
                                Swal.fire({type:"danger",title:"Not Changed!",text:"Status has been not changed.",confirmButtonClass:"btn btn-danger",timer: 1000});
                                if(table=="contractor"){
                                    window.location.href="contractor-list";
                                }else if(table=="staff"){
                                    window.location.href="staff-list";
                                }else if(table=="employee"){
                                    window.location.href="employee-list";
                                }else if(table=="company"){
                                    window.location.href="company-list";
                                }else if(table=="contract"){
                                    window.location.href="contract-list";
                                }else if(table=="job"){
                                    window.location.href="job-list";
                                }else if(table=="standard-job"){
                                    window.location.href="standard-job-list";
                                }else if(table=="exam-question"){
                                    window.location.href="exam-question-list";
                                }else if(table=="candidate"){
                                    window.location.href="candidate-list";
                                }
                            }
                        }
                 });
                    

                }else{
                    Swal.fire({type:"danger",title:"Not Changed!",text:"Your order status has been not changed.",confirmButtonClass:"btn btn-danger",timer: 1000});
                                if(table=="contractor"){
                                    window.location.href="contractor-list";
                                }else if(table=="staff"){
                                    window.location.href="staff-list";
                                }else if(table=="employee"){
                                    window.location.href="employee-list";
                                }else if(table=="company"){
                                    window.location.href="company-list";
                                }else if(table=="contract"){
                                    window.location.href="contract-list";
                                }else if(table=="job"){
                                    window.location.href="job-list";
                                }else if(table=="standard-job"){
                                    window.location.href="standard-job-list";
                                }else if(table=="exam-question"){
                                    window.location.href="exam-question-list";
                                }else if(table=="candidate"){
                                    window.location.href="candidate-list";
                                }
                }
            });
    }



    </script>


    <script type="text/javascript">
        function maritalstatus(val){
            if(val=="single"){
                $("#marriage_date").addClass("d-none").removeClass("d-block");
                $("#husband").addClass("d-none").removeClass("d-block");
                $("#wife").addClass("d-none").removeClass("d-block");
                $("#sons").addClass("d-none").removeClass("d-block");
                $("#daughters").addClass("d-none").removeClass("d-block");
            }else if(val=="married"){
                $("#marriage_date").addClass("d-block").removeClass("d-none");
                $("#husband").addClass("d-block").removeClass("d-none");
                $("#wife").addClass("d-block").removeClass("d-none");
                $("#sons").addClass("d-block").removeClass("d-none");
                $("#daughters").addClass("d-block").removeClass("d-none");
            }
        }
        function replacement_contract(val){
            if(val=="yes"){
                $("#old_contract_no").addClass("d-block").removeClass("d-none");
            }else if(val=="no"){
                $("#old_contract_no").addClass("d-none").removeClass("d-block");
            }
        }
        function contract_full_info(id){
            $.ajax({
                method:'POST',
                url:"{{url('contract_full_info')}}",
                data:"_token={{csrf_token()}}&id="+id,
                success:function(data){
                    $("#contract_full_info").html(data);
                }
            });
        }
        function standard_job_info(id){
            $.ajax({
                method:'POST',
                url:"{{url('standard_job_info')}}",
                data:"_token={{csrf_token()}}&id="+id,
                success:function(data){
                    $("#standard_job_info").html(data);
                }
            });
        }
        function job_full_info(id){
            $.ajax({
                method:'POST',
                url:"{{url('job_full_info')}}",
                data:"_token={{csrf_token()}}&id="+id,
                success:function(data){
                    $("#job_full_info").html(data);
                }
            });
        }
        function employee_full_info(id){
            $.ajax({
                method:'POST',
                url:"{{url('employee_full_info')}}",
                data:"_token={{csrf_token()}}&id="+id,
                success:function(data){
                    $("#employee_full_info").html(data);
                }
            });
        }
        function get_salary(){
              var location_area=$("#location_area").val();
              var standard_job_id=$("#standard_job_id").val();
                $.ajax({
                    method:'POST',
                    url:"{{url('get_salary')}}",
                    data:"_token={{csrf_token()}}&location_area="+location_area+"&standard_job_id="+standard_job_id,
                    success:function(data){
                        $("#monthly_salary").val(data);
                        $("#annual_salary").val(data * 12);
                    }
                });     
        }
        function get_staff_for(id,opr,oprid){
            if(id!=""){
                $.ajax({
                    method:'POST',
                    url:"{{url('get-staff-for')}}",
                    data:"_token={{csrf_token()}}&id="+id+"&opr="+opr+"&oprid="+oprid,
                    success:function(data){
                        $('#under_id').html(data);
                    }
                });     
            }
        }
         function get_role_for_staff(id,opr,oprid){
            if(id!=""){
                $.ajax({
                    method:'POST',
                    url:"{{url('get-role-for-staff')}}",
                    data:"_token={{csrf_token()}}&id="+id+"&opr="+opr+"&oprid="+oprid,
                    success:function(data){
                        $('#role').html(data);
                    }
                });     
            }
        }
        function annual_performace(){
            var work_quality=parseInt($("#work_quality").val());
            var self_management=parseInt($("#self_management").val());
            var team_work=parseInt($("#team_work").val());
            var attitude_discipline=parseInt($("#attitude_discipline").val());
            var health_safety=parseInt($("#health_safety").val());
            var overall_rating=(work_quality+self_management+team_work+attitude_discipline+health_safety)/5;
            var performance=0;
            var incentive_bonus=0;
            var annual_merit_increment=0;
            var annual_airfare=0;
            if(overall_rating>80){
                performance="Excellent";
                incentive_bonus=1;
                annual_merit_increment=40;
                annual_airfare="Eligible";
            }else if(overall_rating>60){
                performance="Very Good";
                incentive_bonus=1;
                annual_merit_increment=30;
                annual_airfare="Eligible";
            }else if(overall_rating>40){
                performance="Good";
                incentive_bonus=0.75;
                annual_merit_increment=20;
                annual_airfare="Eligible";
            }else if(overall_rating>20){
                performance="Fair";
                incentive_bonus=0.5;
                annual_merit_increment=10;
                annual_airfare="Not Eligible";
            }else if(overall_rating>=0){
                performance="Poor";
                incentive_bonus=0;
                annual_merit_increment=0;
                annual_airfare="Not Eligible";
            }
            $("#overall_rating").val(overall_rating);
            $("#performance").val(performance);
            $("#incentive_bonus").val(incentive_bonus);
            $("#annual_merit_increment").val(annual_merit_increment);
            $("#annual_airfare").val(annual_airfare);

        }
    </script>
    @stack('script')
    
</body>
<!-- END: Body-->

</html>