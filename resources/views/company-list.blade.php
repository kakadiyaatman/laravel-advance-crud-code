@include('header')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
       
                <!-- Scroll - horizontal and vertical table -->
                <section id="headers">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"><i class="feather icon-list"></i> List Of Company </h4>
                                    <a href="{{url('company-add')}}" class="float-right pl-1 pr-1 btn btn-primary"><i class="feather icon-plus"></i> &nbsp;ADD NEW</a>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                        @if(Session::has('message'))
                                            @if(Session('status')==1)
                                            <p class="alert alert-success">{{ Session('message')}}</p>
                                            @else
                                            <p class="alert alert-danger">{{ Session('message')}}</p>
                                            @endif
                                        @endisset

                                            <table class="table table-striped table-bordered complex-headers">
                                                <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                        <th>Company Id</th>
                                                        <th>Company Name</th>
                                                        <th>Directorate</th>
                                                        <th>Group Name</th>
                                                        <th>Team</th>
                                                        <th>Team Leader</th>
                                                        <th>Focal Point</th>
                                                        <th>Company Notes</th>
                                                        <th>Created At</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $item)
                                                    <tr id="rowtr{{$item->company_id}}">
                                                        <td>
                                                            @if($item->status==1)
                                                            <div class="d-flex justify-content-start flex-wrap">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                  <input type="checkbox" id="switch_status{{$item->company_id}}"  class="custom-control-input" checked="" onchange="activeblock('company',{{$item->company_id}},0)">
                                                                  <label class="custom-control-label" for="switch_status{{$item->company_id}}"></label>
                                                                </div>
                                                            </div>
                                                            @else
                                                            <div class="d-flex justify-content-start flex-wrap">
                                                                <div class="custom-control custom-switch mr-2 mb-1">
                                                                  <input type="checkbox" id="switch_status{{$item->company_id}}"  class="custom-control-input" onchange="activeblock('company',{{$item->company_id}},1)">
                                                                  <label class="custom-control-label" for="switch_status{{$item->company_id}}"></label>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href='{{url("company-edit/$item->company_id")}}' >
                                                                <i class="feather icon-edit"  style="font-size: 20px;"></i>
                                                            </a>
                                                            <span onclick="deleteData('company',{{$item->company_id}});" style="cursor: pointer;">
                                                                <i class="feather icon-trash" style="font-size: 20px;color:#7367F0;"></i>
                                                            </span>
                                                        </td>
                                                        <td>#{{$item->company_id}}</td>
                                                        <td>{{$item->name}}</td>
                                                        <td>{{$item->directorate}}</td>
                                                        <td>{{$item->group_name}}</td>
                                                        <td>{{$item->team}}</td>
                                                        <td>{{$item->team_leader}}</td>
                                                        <td>{{$item->focal_point}}</td>
                                                        <td>
                                                            @if(!empty($item->file_notes))
                                                                <a href='{{url("public/images/company/$item->file_notes")}}' target="_blank"><span class="badge badge-primary"><i class="fa fa-external-link"></i> Open</span></a>
                                                            @endif
                                                        </td>
                                                        <td>{{ date("d-m-y h:i:s",strtotime($item->created_at)) }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Scroll - horizontal and vertical table -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

@include('footer')