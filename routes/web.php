<?php
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/',[AdminController::class, 'companyList']);

	Route::get('company-list',[AdminController::class, 'companyList']);
	Route::get('company-add',[AdminController::class, 'companyAddForm']);
	Route::post('company-add',[AdminController::class, 'companyAdd']);
	Route::get('company-edit/{id}',[AdminController::class, 'companyEditForm']);
	Route::post('company-edit',[AdminController::class, 'companyEdit']);


	Route::post('activeblock',[AdminController::class, 'activeBlock']);
	
	Route::post('deletedata',[AdminController::class, 'deleteData']);

	Route::post('export-data',[AdminController::class, 'exportData']);
	Route::get('import-export',[AdminController::class, 'importExportForm']);
	Route::post('import-export',[AdminController::class, 'importExport']);