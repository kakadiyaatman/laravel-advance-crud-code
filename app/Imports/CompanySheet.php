<?php

namespace App\Imports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CompanySheet implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Company([
            'name'    => @$row['name'], 
            'directorate'    => @$row['directorate'],
            'group_name'    => @$row['group_name'],
            'team'    => @$row['team'],
            'team_leader'    => @$row['team_leader'],
            'focal_point'    => @$row['focal_point'],
        ]);
    }
    public function headingRow(): int
    {
        return 1;
    }   
}
