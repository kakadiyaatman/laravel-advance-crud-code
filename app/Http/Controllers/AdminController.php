<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Redirect;
use DB;
use Validator;
use App\Imports\ImportData;
use App\Exports\ExportData;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{

	public function companyList(){
		$active="company-list";
		$data=Company::orderBy('company_id','desc')->get();
		return view('company-list',compact('data','active'));
	}
	public function companyAddForm(){
		$active="company-add";
		return view('company-add',compact('active'));
	}
	public function companyAdd(Request $r){
		$a=new Company();
		$a->name=$r->input('name');
		$a->directorate=$r->input('directorate');
		$a->group_name=$r->input('group_name');
		$a->team=$r->input('team');
		$a->team_leader=$r->input('team_leader');
		$a->focal_point=$r->input('focal_point');
		$file=$r->file('file_notes');
		$file_array = array('image' => $file);
		$rules = array(
        	'image' => 'required|max:10240' // max 10000kb
       	);
		$validator = Validator::make($file_array, $rules);
		if (!$validator->fails())
		{
			$max_id=Company::max('company_id');
			$new_name="company_file_".$max_id.rand(0,100).".".$file->getClientOriginalExtension();
			$destinationPath = 'public/images/company';
			$file->move($destinationPath,$new_name);
			$a->file_notes=$new_name;
			if($a->save()){
				$status=["status"=>1,"message"=>"Successful Added New Record."];
			}else{
				$status=["status"=>0,"message"=>"Try Again Later."];
			}
		}else{
			$status=["status"=>0,"message"=>"File max size is 10 MB"];
		}
		return redirect()->to('company-list')->with($status);
	}
	public function companyEditForm($id){
		$editdata=Company::find($id);
		$active="company-list";
		return view('company-add',compact('active','editdata'));		
	}
	public function companyEdit(Request $r){
		$id=$r->input('id');
		$e=Company::find($id);
		$e->name=$r->input('name');
		$e->directorate=$r->input('directorate');
		$e->group_name=$r->input('group_name');
		$e->team=$r->input('team');
		$e->team_leader=$r->input('team_leader');
		$e->focal_point=$r->input('focal_point');
		$file=$r->file('file_notes');
		$file_array = array('image' => $file);
		$rules = array(
        	'image' => 'required|max:10240'
       	);
		$validator = Validator::make($file_array, $rules);
		if (!$validator->fails())
		{
			$new_name="company_file_".$id.rand(0,100).".".$file->getClientOriginalExtension();
			$destinationPath = 'public/images/company';
			$file->move($destinationPath,$new_name);
			$e->file_notes=$new_name;
		}else{
			$status=["status"=>0,"message"=>"File max size is 10 MB"];
		}
		if($e->save()){
			$status=["status"=>1,"message"=>"Successful Record Edited."];
		}else{
			$status=["status"=>0,"message"=>"Try Again Later."];
		}
		return redirect()->to('company-list')->with($status);
	}

	public function activeBlock(Request $r)
	{
		$id=$r->input('id');
		$table=$r->input('table');
		$status=$r->input('status');
		if($table=='company'){
			$s=Company::find($id);
			$s->status=$status;
			if($s->save()){
				return 1;
			}else{
				return 0;
			}
		}
	}

	function deleteData(Request $r){
		$id=$r->input('id');
		$table=$r->input('table');
		if($table=='company'){
			$d=Company::find($id);
			if($d->delete()){
				echo true;
			}else{
				echo false;
			}
		}
	}

	public function exportData(Request $r){
		return Excel::download(new ExportData, 'HRMS.xlsx');
 	}
	public function importExportForm(){
		$active="import-export";
		return view('import-export',compact('active'));
	}
	public function importExport(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);
        Excel::import(new ImportData, request()->file('import_file'));
        $status=["status"=>1,"message"=>"Imported Successfully."];
   		return redirect()->to('import-export')->with($status);
	}
		

}
