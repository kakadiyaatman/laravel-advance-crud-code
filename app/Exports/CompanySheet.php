<?php

namespace App\Exports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class CompanySheet implements FromQuery,ShouldAutoSize,WithHeadings,WithEvents,WithTitle,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return Company::query();
    }
    public function map($data): array
    {
        return [
            $data->company_id,
            $data->name,
            $data->directorate,
            $data->group_name,
            $data->team,
            $data->team_leader,
            $data->focal_point,
            $data->status,
            $data->updated_at,
            $data->created_at,
           
        ];
    }
    public function headings(): array
    {
        return [
        	'company_id',
        	'name',
        	'directorate',
        	'group_name',
        	'team',
        	'team_leader',
        	'focal_point',
        	'status',
        	'updated_at',
        	'created_at'
        ];
    }
     public function title(): string
    {
        return 'Company';
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class  => function(AfterSheet $event) {
                $event->sheet->getStyle('B1:G1')->applyFromArray(
                    [
                     	'font'=>[
                     		'bold'=>true
                     	],
                     	'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['argb' => 'FF008000'],
                            ],
                        ]
                    ]
                );
            },
        ];
    }
}
