<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table="company";
    protected $primaryKey="company_id";
    protected $fillable = [
        'company_id','name','directorate','group_name','team','team_leader','focal_point','file_notes','status'
    ];
}
